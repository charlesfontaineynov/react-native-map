import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import * as Permissions from "expo-permissions";

/* Default init Map values */
const { width, height } = Dimensions.get( 'window' );
const LATITUDE = 45.243647; 
const LONGITUDE = -0.273616;
const LATITUDE_DELTA = 5;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);

/* Education locations */
const locations = require('./locations.json');

export default class App extends React.Component {

  constructor(props) {

    super(props);

      /* Default values state for latitude/longitude to render Component for mutations in time */
      this.state = {
        latitude: LATITUDE,
        longitudue: LONGITUDE,
        locations: require('./locations.json')
      }
  }

  /* Ask user permissions to geolocate it */
  async componentDidMount() {
    let status = {};
    status = await Permissions.getAsync(Permissions.LOCATION);
    console.log(status);

    if(status !== 'granted') {
      console.log('Permission not granted');
      const response = await Permissions.askAsync(Permissions.LOCATION);
    }
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => this.setState({ latitude, longitude },
      () => console.log('State:', this.state)),
      (error) => console.log('Error:', error)
    )
  }
  
  render() {
    const { latitude, longitude } = this.state;

    if(latitude && longitude) {
      return (
        <MapView
          showsUserLocation
          style = {styles.map}
          initialRegion = {{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
        >
          <Marker
            coordinate = {{
              latitude: LATITUDE,
              longitude: LONGITUDE,
            }}
            title = "Beecoming - Espace N10, 17210 Montlieu-La Garde"
          >
          </Marker>
          
          {/* Display all locations markers */}
          { this.state.locations.map(location => {
            <MapView.Marker 
              coordinate = {{
                latitude: location.coordinate.latitude,
                longitude: location.coordinate.longitude
              }}
              title = {location.name}
              image={require('./img/map-pin.png')} />
          }) }
        </MapView>
      );
    }
    return (
      <View style={styles.container}>
        { (latitude && longitude) ? 
          <Text>Your location is: { latitude }, {longitude}</Text>
          :
          <Text>We need your permission to access your current location all the time.</Text>
        }
      </View>
    );
  }
}

/* Default Init Map Styles  */

const styles = StyleSheet.create({
  container: {
    // ...StyleSheet.absoluteFillObject,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  map: {
    // ...StyleSheet.absoluteFillObject,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  }
});